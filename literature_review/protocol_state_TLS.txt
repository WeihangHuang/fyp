Protocol State Fuzzing of TLS Implementations

General idea: use state machine learning to infer state machines from protocol implementations, using only blackbox testing, and then inspect the inferred state machines to look for spurious behaviour which might be an indication of flaws in the program logic.

Algorithm or Library: LearnLib(Angluin’s L* algorithm), Chow’s W-method

Using LearnLib we need to fix the input alphabet.
