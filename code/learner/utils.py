#!/usr/bin/env python3
"""Utility functions for the project.

Some of them may not be utilities and should be some object method. Refactor if necessary.
"""
import scapy.all as scapy
import constants
import automata


def analyse_traffic(pcap_file_paths):
	all_pkts = scapy.rdpcap(constants.HOST_PCAP_PATH)
	total_pkts = len(all_pkts)

	automata_info = []

	for link, path in pcap_file_paths.items():
		pkts = scapy.rdpcap(path)
		if len(pkts) > 0:
			automata_info.append((link, len(pkts)))

	return automata.Automata(total_pkts=total_pkts, pkts_by_link=automata_info)