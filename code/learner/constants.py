#!/usr/bin/env python3
"""Constants file. Most of them are path information.
"""

HOME_PATH = '/home/mininet/fyp/code'
TOPO_TARGET_PATH = HOME_PATH + '/topo/target_topo.txt'
MININET_HELPER_SCRIPT_PATH = '/home/mininet/mininet/util/m'
CUSTOM_SERVER_PATH = HOME_PATH + '/client_server/run_server.py'
CUSTOM_CLIENT_PATH = HOME_PATH + '/client_server/run_client.py'

# SERVER_LOG_FILE = '/home/mininet/Desktop/log/server_log_{}.txt'.format(time_str)
# CLIENT_LOG_FILE = '/home/mininet/Desktop/log/client_log_{}.txt'.format(time_str)
LOG_DIRECTORY = HOME_PATH + '/log'
SERVER_LOG_FILE = LOG_DIRECTORY + '/server_log.txt'
CLIENT_LOG_FILE = LOG_DIRECTORY + '/client_log.txt'
BASE_PCAP_NAME = '/{src_switch}_{dst_switch}.pcap'
HOST_PCAP_PATH = LOG_DIRECTORY + '/h1.pcap'

BASE_COMMAND = '/home/mininet/mininet/util/m {shell_name} {command}'
BASE_INTERFACE_NAME = '{switch_name}-eth{ingress_port}'
TCPDUMP_BASE_COMMAND = 'tcpdump -i {interface} -w {filename} ip'
SERVER_BASE_COMMAND = ('python3 {server_path} -I {server_ip} -p {listening_port} -P {protocol} '
 						   '-t {timeout}')
CLIENT_BASE_COMMAND = ('python3 {client_path} -I {server_ip} -p {server_port} '
 						   '-n {num_of_message} -P {protocol} -s {src_port}')
# TSHARK_READ_COMMAND = 'tshark -r {infile} > {outfile}'

H2_IP = '10.0.0.2'
H1_IP = '10.0.0.1'

LISTENING_PORT = 65432
SERVER_TIME_OUT = 2
