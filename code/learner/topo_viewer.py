"""The class that offer leaner a globel view of the topology
"""
import networkx as nx

class TopoViewer():
    def __init__(self, topo_file):
        self.graph = nx.Graph()
        self._hosts = []
        self._switches = []
        self._links = []
        
        self.build(topo_file)

    def build(self, topo_file_path):
        # Open file and read lines
        topo_config = []
        with open(topo_file_path, 'r') as f:
            for line in f:
                value = line.partition('#')[0]
                value = value.rstrip()
                
                # Append if it has value
                if value:
                    topo_config.append(value)

        # A helper pointer
        pointer = 0

        # Store information to dicts
        # Get number of hosts:
        num_of_hosts = int(topo_config[pointer])
        for host in topo_config[pointer+1: pointer+num_of_hosts+1]:
            host_name, host_ip, host_mac = host.split()
            unused = host_ip, host_mac
            # add host as node to networkx graph
            self.graph.add_node(host_name)
            self._hosts.append(host_name)

        pointer += num_of_hosts + 1

        # Get number of switches
        num_of_switches = int(topo_config[pointer])
        for switch in topo_config[pointer+1: pointer+num_of_switches+1]:
            self.graph.add_node(switch)
            self._switches.append(switch)

        pointer += num_of_switches + 1

        # Get number of links
        num_of_links = int(topo_config[pointer])
        for link in topo_config[pointer+1: pointer+num_of_links+1]:
            src_node, dst_node, src_port_str, dst_port_str, weight_str = link.split()
            
            # Convert the weight and ports from string to int
            src_port = int(src_port_str)
            dst_port = int(dst_port_str)
            weight = int(weight_str)

            # Add links to the networkx graph first
            self.graph.add_edge(src_node, dst_node, weight=weight)
            self._links.append((src_node, dst_node, src_port, dst_port, weight))

    def get_links(self):
        return self._links
    
    def get_links_between_switches(self):
        return_links = []

        for link in self._links:
            src_node = link[0]
            dst_node = link[1]

            if src_node[0] == 's' and dst_node[0] == 's':
                return_links.append(link)

        return return_links
    
    def get_pkts_direction(self, src_host, dst_host):
        link_directions = {}

        path_generator = nx.all_shortest_paths(G=self.graph,
                                               source=src_host,
                                               target=dst_host,
                                               weight='weight')
        
        for path in path_generator:
            last_node = None
            for node in path:
                if last_node:
                    link_directions[frozenset((last_node, node))] = \
                        "{} -> {}".format(last_node, node)
                last_node = node

        return link_directions
