#!/usr/bin/env python3
"""The class that used to collect network trace
"""
import os
import subprocess
import constants
import logging as logger
import time


class TraceCollector():
    def __init__(self, viewer, init_src_port=60001):
        self.viewer = viewer
        self.init_src_port = init_src_port
        self.last_used_src_port = {}

    def collect_traffic(self, client_ip, server_ip, protocol, runs, num_of_message=2):
        # Get a list of links that need to open a tcpdump monitor.
        with open(constants.TOPO_TARGET_PATH, 'r') as tf:
            topo_config_path = tf.readline()
            topo_config_path = topo_config_path.rstrip("\n\r")

        links = self.viewer.get_links_between_switches()

        with open(constants.SERVER_LOG_FILE, 'w') as server_log, \
            open(constants.CLIENT_LOG_FILE, 'w') as client_log:
            pcap_file_paths = {}
            tcpdump_processes = []

            # Open run tcpdump for each link, and store the process variable in a list
            # A link is a tuple of (node_one, node_two, port_one, port_two, weight)
            for link in links:
                pcap_file_name = constants.BASE_PCAP_NAME.format(src_switch=link[0],
                                                                 dst_switch=link[1])
                pcap_file_path = constants.LOG_DIRECTORY + pcap_file_name
                pcap_file_paths[(link[0], link[1])] = pcap_file_path
                # pcap_file_paths.append(pcap_file_path)

                interface = constants.BASE_INTERFACE_NAME.format(switch_name=link[0],
                                                                 ingress_port=link[2])
                tcpdump_command = constants.TCPDUMP_BASE_COMMAND.format(interface=interface,
                                                                        filename=pcap_file_path)

                command = constants.BASE_COMMAND.format(shell_name=link[0],
                                                        command=tcpdump_command)
                process = subprocess.Popen(command.split(' '))
                tcpdump_processes.append(process)

            # An extra tcpdump that collect packets between a host and a switch.
            # This is used to get the total number of packet.
            host_tcpdump_command = constants.TCPDUMP_BASE_COMMAND.format(
                interface='s1-eth1', filename=constants.HOST_PCAP_PATH)
            host_command = constants.BASE_COMMAND.format(shell_name='s1',
                                                         command=host_tcpdump_command)
            host_process = subprocess.Popen(host_command.split(' '))
            tcpdump_processes.append(host_process)

            for num_of_run in range(runs):
                logger.info('Current run is run {}'.format(num_of_run + 1))
                # Start Server
                server_command = constants.SERVER_BASE_COMMAND.format(
                    server_path=constants.CUSTOM_SERVER_PATH,
                    server_ip=server_ip,
                    listening_port=constants.LISTENING_PORT,
                    protocol=protocol,
                    timeout=constants.SERVER_TIME_OUT
                )

                h2_command = constants.BASE_COMMAND.format(shell_name='h2', command=server_command)
                server_process = subprocess.Popen(h2_command.split(' '), stdout=server_log)

                # Decide the src port:
                client_port = self.get_src_port(client_ip)
                # wait a small period of time to ensure the server is run first. Then start client
                time.sleep(0.1)
                client_command = constants.CLIENT_BASE_COMMAND.format(
                    client_path=constants.CUSTOM_CLIENT_PATH,
                    server_ip=server_ip,
                    server_port=constants.LISTENING_PORT,
                    num_of_message=num_of_message,
                    protocol=protocol,
                    src_port=client_port
                )
                h1_command = constants.BASE_COMMAND.format(shell_name='h1', command=client_command)
                client_process = subprocess.call(h1_command.split(' '), stdout=client_log)

                # wait for timeout time to ensure the server is shutdown
                time.sleep(constants.SERVER_TIME_OUT)

            # Send SIGINT to the all bashes running tcpdump.
            for process in tcpdump_processes:
                logger.info('kill process with id %s' % (process.pid,))
                os.system('sudo kill -s SIGINT %s' % (process.pid,))
                process.wait()

        logger.info('Traffic collection finished')

        return pcap_file_paths

    def get_src_port(self, client_ip):
        port = self.init_src_port
        last_port = self.last_used_src_port.get(client_ip)
        if last_port:
            if last_port >= 65535:
                print("Used all ports")
                exit(2)
            port = last_port + 1

        self.last_used_src_port[client_ip] = port
        return port
