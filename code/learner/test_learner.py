#!/usr/bin/env python3
import automata
import trace_collector
import constants
import utils
import topo_viewer
# from datetime import datetime

# now = datetime.now()
# time_str = now.strftime("%m_%d_%Y_%H:%M:%S")

if __name__ == "__main__":
	old_automata = automata.Automata(total_pkts=0, pkts_by_link=[])
	new_automata = None
	
	with open(constants.TOPO_TARGET_PATH, 'r') as tf:
            topo_config_path = tf.readline()
            topo_config_path = topo_config_path.rstrip("\n\r")
	
	viewer = topo_viewer.TopoViewer(topo_config_path)
	collector = trace_collector.TraceCollector(viewer=viewer)

	for run in range(1, 10): # hard cap at 10 iterations
		pcap_file_paths = collector.collect_traffic(
			client_ip=constants.H1_IP,
			server_ip=constants.H2_IP,
			protocol='TCP',
			runs=run,
		)
		new_automata = utils.analyse_traffic(pcap_file_paths)
		if automata.same_automata(old_automata, new_automata):
			break
		else:
			old_automata.merge_automata(new_automata)

	pkts_direction = viewer.get_pkts_direction(src_host='h1', dst_host='h2')
	automata_file = constants.LOG_DIRECTORY + '/automata.txt'
	automata_info = old_automata.calculate_pkts_percentage()
	with open(automata_file, 'w') as f:
		for link, ratio in automata_info.items():
			transaction = pkts_direction.get(link)
			f.write("{}: {}\n".format(transaction, ratio))






