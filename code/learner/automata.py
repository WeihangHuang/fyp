#!/usr/bin/env python3
"""The automata class file
"""

class Automata():
    def __init__(self, total_pkts, pkts_by_link):
        """pkts_by_link must be a sequence object. Something like:
            Sequence[((node1, node2), pkts_num)]
        """
        self._total_pkts = total_pkts
        self._pkts_by_link = {}
        for item in pkts_by_link:
            node1, node2 = item[0]
            pkts_num = item[1]

            self._pkts_by_link[frozenset([node1, node2])] = pkts_num

    def get_total_pkts(self):
        return self._total_pkts

    def get_all_links(self):
        return self._pkts_by_link

    def merge_automata(self, automata):
        """Merge with another automata.
        """
        self._total_pkts += automata.get_total_pkts()
        links = automata.get_all_links()

        for link, pkts_num in links.items():
            old_pkts_num = self._pkts_by_link.get(link)
            if old_pkts_num:
                self._pkts_by_link[link] = old_pkts_num + pkts_num
            else:
                self._pkts_by_link[link] = pkts_num

    def calculate_pkts_percentage(self):
        """Calculate the ratio between number of packets of a link to the total
            packets.
        """
        pkts_ratio_by_link = {}
        for link, pkts_num in self.get_all_links().items():
            pkts_ratio = pkts_num / self.get_total_pkts()
            pkts_ratio_by_link[link] = pkts_ratio

        return pkts_ratio_by_link


def same_automata(automata1, automata2):
    """Compare with another automata to check whether they are the 'same'
        The 'same' means two automata have the same shape, and the ratio of
        packets of each transaction is close.
    """
    # verify whether they have the same shape:
    automata1_links = automata1.get_all_links()
    automata2_links = automata2.get_all_links()

    automata1_shape = frozenset(automata1_links.keys())
    automata2_shape = frozenset(automata2_links.keys())

    if automata1_shape != automata2_shape:
        return False

    # then verify the ratio. Two automata are the same if all ratios of links
    # are less then some threshold
    threshold = 0.1

    automata1_ratios = automata1.calculate_pkts_percentage()
    automata2_ratios = automata2.calculate_pkts_percentage()

    for link, ratio1 in automata1_ratios.items():
        ratio2 = automata2_ratios.get(link)

        if abs(ratio1 - ratio2) > threshold:
            return False

    return True
