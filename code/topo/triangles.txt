# This is the configuration file that describes the topology of a network.
#
#
#	======	   ======					  |=======|     |=====|
#	| h1 | --- | s1 | --------------------|  s2   | ----| h2  |
#	======     ======  |------------------|=======|     |=====|
#				|      |    |---------------|	|
#				|  |---|    |				    |	
#			   ======      ======		    ======
#			   | s3 | ---- | s4 | --------- | s5 |
#			   ======      ======           ======
#
# Possible paths: (s1, s2); (s1, s3, s2), (s1, s3, s4, s2), (s1, s3, s4, s5, s2)


# This is the host section. The 1st line is a single number N that indicates the
# number of hosts in total. From the 2nd to N+1 lines are the information of 
# hosts
# The number of hosts
2

# The information of hosts. The format is
# host_name    host_ip    host_mac
# Example:
h1 10.0.0.1 00:00:00:00:00:01
h2 10.0.0.2 00:00:00:00:00:02



# This is the switch section. The 1st line is a single number N that indicates
# the number of switches in total. From the 2nd to N+1 lines are the information
# of switches.
# The number of switches
5

# The information of switches. The format is
# switch_name
# Example:
s1
s2
s3
s4
s5


# This is the link section. The 1st line is a single number N that indicates the
# number of links in total. From the 2nd to N+1 lines are the information of
# links
# The number of links
9

# The information of links. The format is
# src_node    dst_node    src_port    dst_port    weight
# Example:
h1 s1 1 1 1
h2 s2 1 2 1
s1 s2 2 1 4
s1 s3 3 1 1
s2 s3 3 2 3
s2 s4 4 2 2
s2 s5 5 2 1
s3 s4 4 3 1
s4 s5 5 4 1


