# This is the configuration file that describes the topology of a network. 


# This is the host section. The 1st line is a single number N that indicates the
# number of hosts in total. From the 2nd to N+1 lines are the information of 
# hosts
# The number of hosts
# Uncomment this line and enter an integer.

# The information of hosts. The format is
# host_name    host_ip    host_mac
# Example:
# h1 10.0.0.1 00:00:00:00:00:01
# Uncomment, add or remove hosts if needed


# This is the switch section. The 1st line is a single number N that indicates
# the number of switches in total. From the 2nd to N+1 lines are the information
# of switches.
# The number of switches
# Uncomment this line and enter an integer.

# The information of switches. The format is
# switch_name
# Example:
# s1
# Uncomment, add or remove switches if needed


# This is the link section. The 1st line is a single number N that indicates the
# number of links in total. From the 2nd to N+1 lines are the information of
# links
# The number of links
# Uncomment this line and enter an integer.

# The information of links. The format is
# src_node    dst_node    src_port    dst_port    weight
# Example:
# h1 s1 1 1 1
# Uncomment, add or remove links if needed


