#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel, info
from mininet.cli import CLI
from mininet.node import RemoteController, OVSSwitch


class MyTopo(Topo):
    """
    The custom topology
    """
    # Path to the topo config file
    target_config_path = '/home/mininet/fyp/code/topo/target_topo.txt'

    # lists or dicts to store data
    hosts_list = []
    switches_list = []
    links_list = []

    def build(self):
        """
        Create topo based on the lists or dicts as object variable
        """
        # Call the read file method
        self.read_config()

        # Create hosts
        for host_info in self.hosts_list:
            self.addHost(host_info[0], ip=host_info[1], mac=host_info[2])

        # Create switches
        for switch_info in self.switches_list:
            self.addSwitch(switch_info, protocols='OpenFlow13')

        # Add links
        for link_info in self.links_list:
            self.addLink(node1=link_info[0],
                         node2=link_info[1],
                         port1=int(link_info[2]),
                         port2=int(link_info[3])
                         )

    def read_config(self):
        """
        Read and parse the configuration from the topo config file and save values
        to the object variable. This will remove all 
        """
        with open(self.target_config_path, 'r') as tf:
            topo_config_path = tf.readline()
            topo_config_path = topo_config_path.rstrip("\n\r")
        
        topo_config = []
        with open(topo_config_path, 'r') as f:
            for line in f:
            	value = line.partition('#')[0]
            	value = value.rstrip()
            	
            	# Append if it has value
            	if value:
            		topo_config.append(value)


        # Create a helper pointer to get the elements from the main list
        pointer = 0

        # Read the first element which is a int, indicates the number of host in the network.
        num_of_host = int(topo_config[pointer])

        # Get the sub list contains host information from the main list based on the pointer and
        # number of host. Store the host names and their host mac as tuples in object variable
        for host in topo_config[pointer+1: pointer+num_of_host+1]:
            host_name, host_ip, host_mac = host.split()
            self.hosts_list.append((host_name, host_ip, host_mac))

        # Modify the pointer value to point to the element indicates the number of switches
        pointer += num_of_host + 1

        # Read the number of switches
        num_of_switch = int(topo_config[pointer])

        # Store all switches in to a list
        for switch in topo_config[pointer + 1: pointer + 1 + num_of_switch]:
            self.switches_list.append(switch)

        # Modify the pointer value
        pointer += num_of_switch + 1

        # Read the number of links
        num_of_link = int(topo_config[pointer])
        # Finally, add links
        for link in topo_config[pointer + 1: pointer + 1 + num_of_link]:
            node_one, node_two, src_port, dst_port, weight = link.split()
            # We don't care the weight when create topo
            unused = weight

            # add link as a tuple of a pair of nodes
            self.links_list.append((node_one, node_two, src_port, dst_port))

def run_mytopo():
    # Create an instance of topology
    topo = MyTopo()

    # Create a network using OVS and a remote controller
    net = Mininet(
        topo=topo,
        controller=lambda name: RemoteController(name, ip='127.0.0.1'),
        switch=OVSSwitch,
    )

    # Start the network
    net.start()

    # net['s1'].cmd('ovs-vsctl set Bridge s1 protocols=OpenFlow13')
    # net['s2'].cmd('ovs-vsctl set Bridge s2 protocols=OpenFlow13')
    # net['s3'].cmd('ovs-vsctl set Bridge s3 protocols=OpenFlow13')

    ## Enable STP in all switches
    # info('*** Activate STP...\n')
    # net['s1'].cmd('ovs-vsctl set bridge s1 stp-enable=true')
    # net['s2'].cmd('ovs-vsctl set bridge s2 stp-enable=true')
    # net['s3'].cmd('ovs-vsctl set bridge s3 stp-enable=true')

    # Open the Command Line Interface
    CLI(net)

    # Stop and clean the network
    net.stop()

if __name__ == '__main__':
    # This runs if this file is executed directly
    setLogLevel( 'info' )
    run_mytopo()

# Allows the file to be imported using `mn --custom <filename> --topo mytopo`
topos = {
    'mytopo': MyTopo
}

