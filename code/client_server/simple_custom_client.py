#!/usr/bin/env python3

import socket

SERVER_HOST = '10.0.0.2'
SERVER_PORT = 65432

client_port = 60001
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.bind(('0.0.0.0', client_port))
	s.connect((SERVER_HOST, SERVER_PORT))

	s.sendall(b'Hello, world')
	data = s.recv(1024)

print('Received', repr(data))
print('Src port:', client_port )


client_port = 60002
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.bind(('0.0.0.0', client_port))
	s.connect((SERVER_HOST, SERVER_PORT))

	s.sendall(b'Hello, world')
	data = s.recv(1024)

print('Received', repr(data))
print('Src port:', client_port )