#!/usr/bin/env python3
"""Script that start a simple server.

Options of the command is:
	-I, --src-ip: Source IP address. This is the IP address of this 
				  server.
	-p, --src-port: Source port. The server listening port outport
	-h, --help: Help. Print the input format of the command.
	-P: Protocol. Should be only 'TCP' or 'UDP'
	-t, --timeout: the server living time.
"""
import sys
import getopt
import socket

def usage():
	print('\nusage: ' + sys.argv[0] +
		  ' -I <src_ip> -p <src_port> -P <protocol> -t <timeout sec>')


def read_args(argv):
	proto = server_ip = listening_port = timeout = None

	try:
		opts, args = getopt.getopt(
			argv,
			"hI:p:P:t:",
			["src-ip=", "src-port=", "help", "timeout="]
		)
	except getopt.GetoptError as err:
		# print help information and exit:
		print(str(err))
		usage()
		sys.exit(2)

	# Read arguments
	for opt, arg in opts:
		if opt in ('-h', '--help'):
			usage()
			sys.exit()
		elif opt == '-P':
			proto = arg
		elif opt in ('-I', '--src-ip'):
			server_ip = arg
		elif opt in ('-p', '--src-port'):
			listening_port = arg
		elif opt in ('-t', '--timeout'):
			timeout = arg

	# Check whether all arguments are provided:
	if (not proto or
		not listening_port or
		not server_ip or
		not timeout):

		print("Missing arguments")
		print(proto, listening_port, server_ip, timeout)
		sys.exit(2)

	# convert port and number of packets from string to int
	try:
		listening_port = int(listening_port)
		timeout = int(timeout)
	except ValueError as err:
		print(str(err))
		sys.exit(2)

	return proto, server_ip, listening_port, timeout


def run_server(protocol, server_ip, listening_port, timeout):
	# setup socket type based on protocol
	if protocol.upper() == 'TCP':
		run_tcp_server(server_ip, listening_port, timeout)
	elif protocol.upper() == 'UDP':
		run_udp_server(server_ip, listening_port, timeout)
	else:
		print("Unknown protocol. Only TCP and UDP available")
		sys.exit(2)

	print('server close cleanly')


def run_tcp_server(server_ip, listening_port, timeout):
	while True:
		try:
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.bind((server_ip, listening_port))
			sock.settimeout(timeout)
			sock.listen(1)

			conn, addr = sock.accept()
			with conn:
				print('Connected by', addr)
				while True:
					data = conn.recv(1024)
					if not data:
						break
					conn.sendall(data)
		except socket.timeout:
			print('Time out reach.')
			sock.close()
			break


def run_udp_server(server_ip, listening_port, timeout):
	while True:
		try:
			sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
			sock.bind((server_ip, listening_port))
			sock.settimeout(timeout)

			while True:
				data, addr = sock.recvfrom(1024)
				print('Data from ', addr)
				sock.sendto(data, addr)
		except socket.timeout:
			print('Time out reach.')
			sock.close()
			break

if __name__ == "__main__":
	proto, server_ip, listening_port, timeout = read_args(sys.argv[1:]) 
	run_server(protocol=proto, server_ip=server_ip,
			   listening_port=listening_port, timeout=timeout)



