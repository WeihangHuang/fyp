#!/usr/bin/env python3

import socket

HOST = 'localhost' # IP of h2
PORT = 65432

# while True:
# 	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
# 		s.bind((HOST, PORT))
# 		s.settimeout(5)
# 		s.listen(5)
# 		conn, addr = s.accept()
# 		with conn:
# 			print('Connected by', addr)
# 			while True:
# 				data = conn.recv(1024)
# 				if not data:
# 					break
# 				conn.sendall(data)

while True:
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.bind((HOST, PORT))
		sock.settimeout(5)
		sock.listen(1)

		conn, addr = sock.accept()
		with conn:
			print('Connected by', addr)
			while True:
				data = conn.recv(1024)
				if not data:
					break
				conn.sendall(data)
	except socket.timeout:
		print('Time out reach.')
		sock.close()
		break
	except:
		sock.close()
		raise

print('server close cleanly')