#!/usr/bin/env python3
"""Script that starts a client.

Options of the command is:
	-I, --dst-ip: Destination IP address. Should be server ip.
	-p, --dst-port: Destination port number. Should be the server 
					 listening port.
	-h, --help: Help. Print the input format of the command.
	-n: Number of messages.
	-P: Protocol. Should be only 'TCP' or 'UDP'
	-s, --src-port: Source port. The client outport
"""
import sys
import getopt
import socket

def usage():
	print('\nusage: ' + sys.argv[0] +
		  ' -I <dst_ip> -p <dst_port> -n <num_of_messages> -P'
		  ' <protocol> -s <src_port>')

def run_client(protocol, num_of_messages, src_port, dst_ip, dst_port):
	# setup socket type based on protocol
	sock_type = None
	if protocol.upper() == 'TCP':
		sock_type = socket.SOCK_STREAM
	elif protocol.upper() == 'UDP':
		sock_type = socket.SOCK_DGRAM
	else:
		print("Unknown protocol. Only TCP and UDP available")
		sys.exit(2)

	with socket.socket(socket.AF_INET, sock_type) as sock:
		sock.bind(('0.0.0.0', src_port))
		sock.connect((dst_ip, dst_port))

		for i in range(num_of_messages):
			sock.sendall(b'Hello, world')
			data = sock.recv(1024)

			print(i, repr(data))	

def read_args(argv):
	num_of_messages = proto = src_port = server_ip = server_port = None

	try:
		opts, args = getopt.getopt(
			argv,
			"hI:p:n:P:s:",
			["dst-ip=", "dst-port=", "help", "src-port="]
		)
	except getopt.GetoptError as err:
		# print help information and exit:
		print(str(err))
		usage()
		sys.exit(2)

	# Read arguments
	for opt, arg in opts:
		if opt in ('-h', '--help'):
			usage()
			sys.exit()
		elif opt == '-n':
			num_of_messages = arg
		elif opt == '-P':
			proto = arg
		elif opt in ('-I', '--dst-ip'):
			server_ip = arg
		elif opt in ('-p', '--dst-port'):
			server_port = arg
		elif opt in ('-s', '--src-port'):
			src_port = arg

	# Check whether all arguments are provided:
	if (not num_of_messages or
		not proto or
		not src_port or
		not server_ip or
		not server_port):

		print("Missing arguments")
		print(num_of_messages, proto, src_port, server_ip, server_port)
		sys.exit(2)

	# convert port and number of packets from string to int
	try:
		num_of_messages = int(num_of_messages)
		server_port = int(server_port)
		src_port = int(src_port)
	except ValueError as err:
		print(str(err))
		sys.exit(2)

	return proto, num_of_messages, src_port, server_ip, server_port

if __name__ == "__main__":
	proto, num_of_messages, src_port, server_ip, server_port = \
		read_args(sys.argv[1:]) 
	run_client(protocol=proto, num_of_messages=num_of_messages,
			   src_port=src_port, dst_port=server_port,
			   dst_ip=server_ip)

