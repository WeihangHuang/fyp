# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from collections import defaultdict
from enum import Enum

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib import dpid as dpid_lib
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import in_proto
from ryu.lib.packet import udp
from ryu.lib.packet import tcp
from ryu.lib.packet import ether_types
import ryu.app.ofctl.api as api

import networkx as nx

class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)

        # Path to topology config file
        self.target_config_path = '/home/mininet/fyp/code/topo/target_topo.txt'

        self.switch_port_by_src_and_dst_switch = defaultdict(dict)
        self.host_mac_by_host_ip = {}
        self.switch_port_by_switch_dpid_and_host_mac = defaultdict(dict)

        # Store the pair that from host mac to its directly linked switch
        self.switch_dpid_by_host_mac = {}
        self.host_mac_by_host_name = {}

        # The networkx graph instance
        self.Graph = nx.Graph()

        # The cache for shortest paths between two nodes. The key is the tuple of the nodes
        self.paths_cache = defaultdict(list)

        # The dict store numbers that use to perform round-robin. The key is the tuple of the nodes
        self.last_path_index = {}

        self.read_config()

    ################################
    #                              #
    #       Helper Functions       #
    #                              #
    ################################

    @staticmethod
    def switch_dpid_to_switch_name(dpid):
        """
        The helper function that convert a dpid back to its name representation.
        e.g. 1 -> 's1'
        """
        return 's'+str(dpid)

    @staticmethod
    def switch_name_to_switch_dpid(switch_name):
        """
        The helper function that convert a switch name to dpid
        e.g. 's1' -> 1
        """
        return int(switch_name[1:])

    def read_config(self):
        # Open file and read lines
        topo_config = []
        with open(self.target_config_path, 'r') as tf:
            topo_config_path = tf.readline()
            topo_config_path = topo_config_path.rstrip("\n\r")

        with open(topo_config_path, 'r') as f:
            for line in f:
                value = line.partition('#')[0]
                value = value.rstrip()
                
                # Append if it has value
                if value:
                    topo_config.append(value)

        # A helper pointer
        pointer = 0

        # Store information to dicts
        # Get number of hosts:
        num_of_hosts = int(topo_config[pointer])
        for host in topo_config[pointer+1: pointer+num_of_hosts+1]:
            host_name, host_ip, host_mac = host.split()
            # add host as node to networkx graph
            self.Graph.add_node(host_name)
            
            self.host_mac_by_host_name[host_name] = host_mac
            self.host_mac_by_host_ip[host_ip] = host_mac

        pointer += num_of_hosts + 1

        # Get number of switches
        num_of_switches = int(topo_config[pointer])
        for switch in topo_config[pointer+1: pointer+num_of_switches+1]:
            self.Graph.add_node(switch)

        pointer += num_of_switches + 1

        # Get number of links
        num_of_links = int(topo_config[pointer])
        for link in topo_config[pointer+1: pointer+num_of_links+1]:
            src_node, dst_node, src_port_str, dst_port_str, weight_str = link.split()

            # Convert the port and weight from string to int
            src_port = int(src_port_str)
            dst_port = int(dst_port_str)
            weight = int(weight_str)

            # Add links to the networkx graph first
            self.Graph.add_edge(src_node, dst_node, weight=weight)

            # In theory, two hosts cannot have a direct link. So just need to check whether
            # any of them is host.
            if src_node[0] == 'h':
                self.map_host_to_switch(host=src_node,
                                        switch=dst_node,
                                        host_port=src_port,
                                        switch_port=dst_port,)
            elif dst_node[0] == 'h':
                self.map_host_to_switch(host=dst_node,
                                        switch=src_node,
                                        host_port=dst_port,
                                        switch_port=src_port,)
            else:
                # This is the link between two switch
                # Although the link is bi-directed, still use the naming of src and dst
                src_switch_dpid = self.switch_name_to_switch_dpid(src_node)
                dst_switch_dpid = self.switch_name_to_switch_dpid(dst_node)

                # Add port from one switch to other
                self.switch_port_by_src_and_dst_switch[src_switch_dpid][dst_switch_dpid] = src_port
                self.switch_port_by_src_and_dst_switch[dst_switch_dpid][src_switch_dpid] = dst_port

    def map_host_to_switch(self, host, switch, host_port, switch_port):
        """
        A helper function that stores information of a link between a host and a switch.
        Information is stored in object variables.

        :param host: name of the host
        :param switch: name of the switch
        :param host_port: port number in host side(currently no use)
        :param switch_port: port number in switch side
        """
        unused = host_port
        host_mac = self.host_mac_by_host_name.get(host)
        switch_dpid = self.switch_name_to_switch_dpid(switch)

        # Store mac address of the host and switch pair
        self.switch_dpid_by_host_mac[host_mac] = switch_dpid

        # Store the port from switch side to host
        self.switch_port_by_switch_dpid_and_host_mac[switch_dpid][host_mac] = switch_port

    def calculate_shortest_paths(self, src, dst):
        """
        Find and return a list of shortest paths using networkx

        :param src: source node
        :param dst: target node
        :return: A list of path
        """
        path_generator = nx.all_shortest_paths(G=self.Graph,
                                               source=src,
                                               target=dst,
                                               weight='weight')
        return [path for path in path_generator]

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    def choose_path(self, src_node, dst_node, ip_proto_num):
        # Check if the cache has paths.
        available_paths = self.paths_cache.get((src_node, dst_node))

        # If no, compute the shortest paths and store in cache
        if not available_paths:
            available_paths = self.calculate_shortest_paths(src_node, dst_node)
            self.paths_cache[(src_node, dst_node)] = available_paths

            # Also we want to store the reverse
            reverse_available_paths = [path[::-1] for path in available_paths]
            self.paths_cache[(dst_node, src_node)] = reverse_available_paths

        # Get last used path index
        last_used_path_index = self.last_path_index.get((src_node, dst_node), -1)

        available_paths_num = len(available_paths)

        # If only one path or not tcp, return it.
        if available_paths_num == 1 or ip_proto_num != in_proto.IPPROTO_TCP:
            path = available_paths[0]
        else:
            # More than one path and using tcp, doing round-robin
            new_path_index = last_used_path_index + 1
            if new_path_index == available_paths_num:
                new_path_index = 0

            path = available_paths[new_path_index]

            # Update the new path index for both forward and backward
            self.last_path_index[(src_node, dst_node)] = new_path_index
            self.last_path_index[(dst_node, src_node)] = new_path_index

        self.logger.info("The path chosen is:")
        self.logger.info("***  {}  ***".format(tuple(path)))
        return path


    def install_path(self, parser, path, in_proto_num, init_in_port, src_mac, dst_mac, src_ip,
                     dst_ip, tcp_src_port=None, tcp_dst_port=None):
        """
        Install the given path to all switches involved.

        This function will install path for both directions.
        i.e. if the given path is ['s1', 's3', 's2'], this function will install both paths
        s1->s3->s2 and s2->s3->s1 to all three switches appear this path.

        The default priority is 1 and is used for all non-tcp packet. Matching field for this case
        only includes in_port, src_mac and dst_mac

        For tcp packet, the priority is 255 and the matching field includes in_port, src and dst
        mac, ethetype, ip_proto, src and dst ipv4 addr, src and dst tcp port

        :param parser: The OpenFlow Parser
        :param path: The path that need to be installed
        :type path: Sequence[String]
        :param in_proto_num: Internet Protocol Number.
        :type in_proto_num: ryu.lib.packet.in_proto object
        :param init_in_port: The initial in port of the packet
        :type init_in_port: Int
        :param src_mac: the source mac address
        :type src_mac: String
        :param dst_mac: the destination mac address
        :type dst_mac: String
        :param src_ip: The ipv4 address of source host.
        :type src_ip: String
        :param dst_ip: The ipv4 address of destination host.
        :type dst_ip: String
        :param tcp_src_port: The source port of tcp packets. Only provided when is_tcp flag is
                             set to True.
        :type tcp_src_port: Int
        :param tcp_dst_port: The destination port of tcp packets. Only provided when is_tcp flag is
                             set to True.
        :type tcp_dst_port: Int
        """
        self.logger.info("Path installation started...")

        path_length = len(path)
        in_port = init_in_port

        for i in range(path_length):
            current_dpid = self.switch_name_to_switch_dpid(path[i])

            # Check whether the current switch is the last one. If yes, next hop is the host
            if i == (path_length - 1):
                out_port = self.switch_port_by_switch_dpid_and_host_mac[current_dpid][dst_mac]
                next_in_port = out_port
            else:
                next_dpid = self.switch_name_to_switch_dpid(path[i+1])
                out_port = self.switch_port_by_src_and_dst_switch[current_dpid][next_dpid]
                next_in_port = self.switch_port_by_src_and_dst_switch[next_dpid][current_dpid]

            actions_forward = [parser.OFPActionOutput(out_port)]
            actions_backward = [parser.OFPActionOutput(in_port)]

            if in_proto_num is in_proto.IPPROTO_TCP:
                # If is_tcp flag is set to True, priority is 255 and has more matching fields
                match_forward = parser.OFPMatch(in_port=in_port,
                                                eth_src=src_mac,
                                                eth_dst=dst_mac,
                                                eth_type=0x0800,
                                                ip_proto=in_proto_num,
                                                ipv4_src=src_ip,
                                                ipv4_dst=dst_ip,
                                                tcp_src=tcp_src_port,
                                                tcp_dst=tcp_dst_port)
                match_backward = parser.OFPMatch(in_port=out_port,
                                                 eth_src=dst_mac,
                                                 eth_dst=src_mac,
                                                 eth_type=0x0800,
                                                 ip_proto=in_proto_num,
                                                 ipv4_src=dst_ip,
                                                 ipv4_dst=src_ip,
                                                 tcp_src=tcp_dst_port,
                                                 tcp_dst=tcp_src_port)
                priority = 255
            else:
                # If not is_tcp, priority is 1 and has less matching fields
                # Namely we don't include tcp src and dst port.
                match_forward = parser.OFPMatch(in_port=in_port,
                                                eth_src=src_mac,
                                                eth_dst=dst_mac,
                                                eth_type=0x0800,
                                                ip_proto=in_proto_num,
                                                ipv4_src=src_ip,
                                                ipv4_dst=dst_ip)
                match_backward = parser.OFPMatch(in_port=out_port,
                                                 eth_src=dst_mac,
                                                 eth_dst=src_mac,
                                                 eth_type=0x0800,
                                                 ip_proto=in_proto_num,
                                                 ipv4_src=dst_ip,
                                                 ipv4_dst=src_ip)
                priority = 1

            # Install the path to the switch.
            # Get datapath object
            datapath = api.get_datapath(self, current_dpid)

            self.add_flow(datapath=datapath,
                          priority=priority,
                          match=match_forward,
                          actions=actions_forward)
            self.add_flow(datapath=datapath,
                          priority=priority,
                          match=match_backward,
                          actions=actions_backward)

            # Update in port
            in_port = next_in_port

        self.logger.info("Path installation completed!")

    ################################
    #                              #
    #  Packets Handling Functions  #
    #                              #
    ################################

    def handle_arp(self, datapath, pkt, eth, in_port):
        arp_packet = pkt.get_protocol(arp.arp)

        # From switch, send the ARP REPLY back to host directly
        if arp_packet.opcode == arp.ARP_REQUEST:
            # Figure out the correct mac address:
            reply_src_ip = arp_packet.dst_ip
            reply_src_mac = self.host_mac_by_host_ip[reply_src_ip]

            # Reply destination info
            reply_dst_mac = eth.src
            reply_dst_ip = arp_packet.src_ip

            self.logger.info(
                "Source host with IP address {src_ip} and MAC address {src_mac} sent an ARP "
                "Request on IP address {dst_ip}".format(
                src_ip=reply_dst_ip, src_mac=reply_dst_mac, dst_ip=reply_src_ip)
            )
            self.logger.info("Resolved MAC address is {}".format(reply_src_mac))

            eth_proto = ethernet.ethernet(reply_dst_mac, reply_src_mac, ether_types.ETH_TYPE_ARP)
            arp_proto = arp.arp(opcode=arp.ARP_REPLY,
                                src_mac=reply_src_mac,
                                src_ip=reply_src_ip,
                                dst_mac=reply_dst_mac,
                                dst_ip=reply_dst_ip)
            reply_pkt = packet.Packet()
            reply_pkt.add_protocol(eth_proto)
            reply_pkt.add_protocol(arp_proto)
            reply_pkt.serialize()

            ofproto = datapath.ofproto
            parser = datapath.ofproto_parser
            self.logger.info("Send ARP Reply Packet...")
            datapath.send_msg(parser.OFPPacketOut(
                datapath=datapath,
                buffer_id=ofproto.OFP_NO_BUFFER,
                in_port=ofproto.OFPP_CONTROLLER,
                actions=[parser.OFPActionOutput(in_port) ],
                data=reply_pkt.data)
            )
            self.logger.info("ARP Reply sent.")
            self.logger.info("ARP packet handling completed!\n")

    def handle_ipv4(self, data, datapath, pkt, eth, in_port):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        ip_pkt = pkt.get_protocol(ipv4.ipv4)

        src_host_mac = eth.src
        dst_host_mac = eth.dst
        dst_dpid = self.switch_dpid_by_host_mac[dst_host_mac]
        current_dpid = datapath.id

        # Check if current switch is the dst switch, i.e. the dst host is link to current switch
        # directly
        if current_dpid == dst_dpid:
            # If yes, we just install the flow from switch to host and forward the packet
            out_port = self.switch_port_by_switch_dpid_and_host_mac[current_dpid][dst_host_mac]

            actions = [parser.OFPActionOutput(out_port)]
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst_host_mac)

            self.add_flow(datapath=datapath, priority=255, match=match, actions=actions)
        else:
            # If no, we first find out the ip protocol of the packet, and do load balancing on TCP
            ip_proto_num = ip_pkt.proto

            current_switch_name = self.switch_dpid_to_switch_name(current_dpid)
            dst_switch_name = self.switch_dpid_to_switch_name(dst_dpid)

            # Choose a path
            self.logger.info("Choose a path...")
            path = self.choose_path(src_node=current_switch_name,
                                    dst_node=dst_switch_name,
                                    ip_proto_num=ip_proto_num)

            tcp_src_port = None
            tcp_dst_port = None

            # Check whether is a tcp packet
            if ip_proto_num == in_proto.IPPROTO_TCP:
                self.logger.info("The packet is using TCP, use load balancing...")

                tcp_pkt = pkt.get_protocol(tcp.tcp)
                tcp_src_port = tcp_pkt.src_port
                tcp_dst_port = tcp_pkt.dst_port

            else:
                self.logger.info("The packet is not using TCP, use default path...")

            self.install_path(parser=parser,
                              path=path,
                              in_proto_num=ip_proto_num,
                              init_in_port=in_port,
                              src_mac=src_host_mac,
                              dst_mac=dst_host_mac,
                              src_ip=ip_pkt.src,
                              dst_ip=ip_pkt.dst,
                              tcp_src_port=tcp_src_port,
                              tcp_dst_port=tcp_dst_port)

            # Determine the out_port for current switch
            next_switch_name = path[1]
            next_switch_dpid = self.switch_name_to_switch_dpid(next_switch_name)

            out_port = self.switch_port_by_src_and_dst_switch[current_dpid][next_switch_dpid]
            actions = [parser.OFPActionOutput(out_port)]

        self.logger.info("Forward the packet...")
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=in_port,
                                  actions=actions,
                                  data=data)
        datapath.send_msg(out)
        self.logger.info("The packet is sent!")
        self.logger.info("IPv4 handling completed!\n")

    def handle_default(self, data, datapath, pkt, eth, in_port):
        unused = pkt
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        dst_host_mac = eth.dst
        dst_dpid = self.switch_dpid_by_host_mac[dst_host_mac]
        current_dpid = datapath.id

        # Check if current switch is the dst switch, i.e. the dst host is link to current switch
        # directly
        if current_dpid == dst_dpid:
            # If yes, we just install the flow from switch to host and forward the packet
            out_port = self.switch_port_by_switch_dpid_and_host_mac[current_dpid][dst_host_mac]

            actions = [parser.OFPActionOutput(out_port)]
        else:
            current_switch_name = self.switch_dpid_to_switch_name(current_dpid)
            dst_switch_name = self.switch_dpid_to_switch_name(dst_dpid)

            # Choose a path. We use a unassigned ip_proto_number(Since we don't really care)
            path = self.choose_path(src_node=current_switch_name,
                                    dst_node=dst_switch_name,
                                    ip_proto_num=252)
            next_switch_name = path[1]
            next_switch_dpid = self.switch_name_to_switch_dpid(next_switch_name)

            out_port = self.switch_port_by_src_and_dst_switch[current_dpid][next_switch_dpid]
            actions = [parser.OFPActionOutput(out_port)]

        self.logger.info("Forward the packet...")
        out = parser.OFPPacketOut(datapath=datapath,
                                  buffer_id=ofproto.OFP_NO_BUFFER,
                                  in_port=in_port,
                                  actions=actions,
                                  data=data)
        datapath.send_msg(out)
        self.logger.info("The packet is sent!")
        self.logger.info("Packet handling completed!\n")

    ################################
    #                              #
    # OFP Events Handling Functions#
    #                              #
    ################################

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        data = msg.data
        datapath = msg.datapath
        in_port = msg.match['in_port']
        dpid = datapath.id

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        arp_pkt = pkt.get_protocol(arp.arp)
        ip_pkt = pkt.get_protocol(ipv4.ipv4)

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return

        # Handle ARP packet:
        if isinstance(arp_pkt, arp.arp):
            self.logger.info("Switch {} received an ARP packet.".format(dpid))
            self.logger.info("Handling the ARP packet...")
            self.handle_arp(datapath=datapath, pkt=pkt, eth=eth, in_port=in_port)
            return

        if isinstance(ip_pkt, ipv4.ipv4):
            self.logger.info("Switch {} received an IPv4 packet.".format(dpid))
            self.logger.info("Handling the IPv4 packet...")
            self.handle_ipv4(data=data, datapath=datapath, pkt=pkt, eth=eth, in_port=in_port)
            return

        # Handle other type of packets
        self.logger.info("Switch {} received a packet doesn't need to handling specially, "
                         "no flow will be installed for it.")
        self.logger.info("Handling the packet...")
        self.handle_default(data=data, datapath=datapath, pkt=pkt, eth=eth, in_port=in_port)
        return
